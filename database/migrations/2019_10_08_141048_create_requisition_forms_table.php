<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRequisitionFormsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('requisition_forms', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->date('date_needed');
            $table->longText('department');
            $table->longText('purpose');
            $table->longText('vendor_name');
            $table->longText('vendor_web_address');
            $table->longText('product_code');
            $table->longText('unit_price');
            $table->integer('qty');
            $table->longText('payments');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('requisition_forms');
    }
}
