
@role('Wine')
<li class="treeview">
	<a href="#">
	      <i class="fa fa-glass"></i>
	      <span>Travelling Wine</span>
		<i class="fa fa-angle-left pull-right"></i>
	</a>
		<ul class="treeview-menu">
		<li class="{{ Request::is('requisitionForms*') ? 'active' : '' }}">
			<a href="{!! route('requisitionForms.create') !!}"><i class="fa fa-edit"></i><span>Create Requisition Order</span></a>
		</li>	            
		</ul>
</li>
@endrole


@role('Freestart')
<li class="treeview">
	<a href="#">
	      <i class="fa fa-globe"></i>
	      <span>Freestart</span>
		<i class="fa fa-angle-left pull-right"></i>
	</a>
		<ul class="treeview-menu">
		<li class="{{ Request::is('requisitionForms*') ? 'active' : '' }}">
			<a href="{!! route('requisitionForms.create') !!}"><i class="fa fa-edit"></i><span>Create Requisition Order</span></a>
		</li>	             
		</ul>
</li>
@endrole


@role('Planning')
<li class="{{ Request::is('requisitionForms*') ? 'active' : '' }}">
	<a href="{!! route('requisitionForms.create') !!}"><i class="fa fa-edit"></i><span>Create Requisition Order</span></a>
</li>	           
@endrole

{{-- Admin Menu --}}
@role('Admin') {{-- Laravel-permission blade helper --}}
{{--
<li class="{{ Request::is('invento') ? 'active' : '' }}">
    <a href="{!! route('invento.index') !!}"><i class="fa fa-edit"></i><span>Sync Products</span></a>
</li>
--}}

<li class="treeview">
	<a href="#">
	      <i class="fa fa-glass"></i>
	      <span>Travelling Wine</span>
		<i class="fa fa-angle-left pull-right"></i>
	</a>
		<ul class="treeview-menu">
			<li class="{{ Request::is('inventories*') ? 'active' : '' }}">
                <a href="{!! route('inventories.index') !!}"><i class="fa fa-list-alt"></i><span>Inventory</span></a>
		</li>  
		<li class="{{ Request::is('requisitionForms*') ? 'active' : '' }}">
			<a href="{!! route('requisitionForms.create') !!}"><i class="fa fa-edit"></i><span>Create Requisition Order</span></a>
		</li>	              
		</ul>
</li>

<li class="treeview">
	<a href="#">
	      <i class="fa fa-globe"></i>
	      <span>Freestart</span>
		<i class="fa fa-angle-left pull-right"></i>
	</a>
		<ul class="treeview-menu">
			<li class="{{ Request::is('requisitionForms*') ? 'active' : '' }}">
				<a href="{!! route('requisitionForms.create') !!}"><i class="fa fa-edit"></i><span>Create Requisition Order</span></a>
			</li>	         
		</ul>
</li>

<li class="treeview">
	<a href="#">
	      <i class="fa fa-book"></i>
	      <span>Planning Handbook</span>
		<i class="fa fa-angle-left pull-right"></i>
	</a>
		<ul class="treeview-menu">
			<li class="{{ Request::is('requisitionForms*') ? 'active' : '' }}">
				<a href="{!! route('requisitionForms.create') !!}"><i class="fa fa-edit"></i><span>Create Requisition Order</span></a>
			</li>	       
		</ul>
</li>

<li class="{{ Request::is('requisitionForms*') ? 'active' : '' }}">
	<a href="{!! route('requisitionForms.index') !!}"><i class="fa fa-edit"></i><span>Requisition Forms</span></a>
</li>	 
<li class="treeview">
	<a href="#">
	      <i class="fa fa-gears"></i>
	      <span>Admin Settings</span>
		<i class="fa fa-angle-left pull-right"></i>
	</a>
		<ul class="treeview-menu">
			<li class="{{ Request::is('users') ? 'active' : '' }}">
                <a href="{!! route('users.index') !!}"><i class="fa fa-user"></i><span>Users</span></a>
            </li>
            <li class="{{ Request::is('roles') ? 'active' : '' }}">
                <a href="{!! route('roles.index') !!}"><i class="fa fa-key"></i><span>Roles</span></a>
            </li> 
            <li class="{{ Request::is('permissions') ? 'active' : '' }}">
                <a href="{!! route('permissions.index') !!}"><i class="fa fa-key"></i><span>Permissions</span></a>
            </li>
		</ul>
</li>
 
@endrole


