<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $inventory->id !!}</p>
</div>

<!-- Sku Field -->
<div class="form-group">
    {!! Form::label('sku', 'Sku:') !!}
    <p>{!! $inventory->sku !!}</p>
</div>

<!-- Product Title Field -->
<div class="form-group">
    {!! Form::label('product_title', 'Product Title:') !!}
    <p>{!! $inventory->product_title !!}</p>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', 'Description:') !!}
    <p>{!! $inventory->description !!}</p>
</div>

<!-- Short Description Field -->
<div class="form-group">
    {!! Form::label('short_description', 'Short Description:') !!}
    <p>{!! $inventory->short_description !!}</p>
</div>

<!-- Price Field -->
<div class="form-group">
    {!! Form::label('price', 'Price:') !!}
    <p>{!! $inventory->price !!}</p>
</div>

<!-- Sale Price Field -->
<div class="form-group">
    {!! Form::label('sale_price', 'Sale Price:') !!}
    <p>{!! $inventory->sale_price !!}</p>
</div>

<!-- Featured Image Field -->
<div class="form-group">
    {!! Form::label('featured_image', 'Featured Image:') !!}
    <p>{!! $inventory->featured_image !!}</p>
</div>

<!-- Gallery Images Field -->
<div class="form-group">
    {!! Form::label('gallery_images', 'Gallery Images:') !!}
    <p>{!! $inventory->gallery_images !!}</p>
</div>

