@extends('layouts.app')

@section('css')

<style>
img.product_image {
    vertical-align: middle;
    margin: 0 auto;
    text-align: center;
    display: -webkit-box;
    width: 50px;
}

div#dataTableBuilder_filter {
    float: right;
}

div#dataTableBuilder_paginate {
    float: right;
}

</style>

@endsection

@section('content')
    <section class="content-header">
        <h1 class="pull-left">Inventory</h1>
        <h1 class="pull-right">
           <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{!! route('inventories.create') !!}">Add New</a>
        </h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                    @include('wine.inventories.table')
            </div>
        </div>
        <div class="text-center">
        
        </div>
    </div>
@endsection

