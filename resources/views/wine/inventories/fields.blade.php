<!-- Sku Field -->
<div class="form-group col-sm-3">
    {!! Form::label('sku', 'Sku:') !!}
    {!! Form::text('sku', null, ['class' => 'form-control']) !!}
</div>

<!-- Product Title Field -->
<div class="form-group col-sm-3">
    {!! Form::label('product_title', 'Product Title:') !!}
    {!! Form::text('product_title', null, ['class' => 'form-control']) !!}
</div>

<!-- Product Title Field -->
<div class="form-group col-sm-6">
    {!! Form::label('stock_qty', 'Stock QTY:') !!}
    {!! Form::text('stock_qty', null, ['class' => 'form-control']) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('description', 'Description:') !!}
    {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
</div>

<!-- Short Description Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('short_description', 'Short Description:') !!}
    {!! Form::textarea('short_description', null, ['class' => 'form-control']) !!}
</div>

<!-- Price Field -->
<div class="form-group col-sm-6">
    {!! Form::label('price', 'Price:') !!}
    {!! Form::number('price', null, ['class' => 'form-control', 'step' => '0.01']) !!}
</div>

<!-- Sale Price Field -->
<div class="form-group col-sm-6">
    {!! Form::label('sale_price', 'Sale Price:') !!}
    {!! Form::number('sale_price', null, ['class' => 'form-control', 'step' => '0.01']) !!}
</div>

<div class="form-group">
    <label for="image">Featured Image</label>
    <input type="file" class="form-control" name="featured_image" id="image" {{ $errors->has('image') ? 'class=has-error' : '' }}>
</div>

<div class="form-group">
    <label for="image">Featured Image</label>
    <input type="file" class="form-control" name="gallery_images" id="image" {{ $errors->has('image') ? 'class=has-error' : '' }}>
</div>

<!-- Submit Field -->
<div class="form-group">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('inventories.index') !!}" class="btn btn-default">Cancel</a>
</div>
