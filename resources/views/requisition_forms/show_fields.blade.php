<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $requisitionForm->id !!}</p>
</div>

<!-- User Id Field -->
<div class="form-group">
    {!! Form::label('user_id', 'User Id:') !!}
    <p>{!! $requisitionForm->user_id !!}</p>
</div>

<!-- Date Needed Field -->
<div class="form-group">
    {!! Form::label('date_needed', 'Date Needed:') !!}
    <p>{!! $requisitionForm->date_needed !!}</p>
</div>

<!-- Department Field -->
<div class="form-group">
    {!! Form::label('department', 'Department:') !!}
    <p>{!! $requisitionForm->department !!}</p>
</div>

<!-- Purpose Field -->
<div class="form-group">
    {!! Form::label('purpose', 'Purpose:') !!}
    <p>{!! $requisitionForm->purpose !!}</p>
</div>

<!-- Vendor Name Field -->
<div class="form-group">
    {!! Form::label('vendor_name', 'Vendor Name:') !!}
    <p>{!! $requisitionForm->vendor_name !!}</p>
</div>

<!-- Vendor Web Address Field -->
<div class="form-group">
    {!! Form::label('vendor_web_address', 'Vendor Web Address:') !!}
    <p>{!! $requisitionForm->vendor_web_address !!}</p>
</div>

<!-- Product Code Field -->
<div class="form-group">
    {!! Form::label('product_code', 'Product Code:') !!}
    <p>{!! $requisitionForm->product_code !!}</p>
</div>

<!-- Unit Price Field -->
<div class="form-group">
    {!! Form::label('unit_price', 'Unit Price:') !!}
    <p>{!! $requisitionForm->unit_price !!}</p>
</div>

<!-- Qty Field -->
<div class="form-group">
    {!! Form::label('qty', 'Qty:') !!}
    <p>{!! $requisitionForm->qty !!}</p>
</div>

<!-- Payments Field -->
<div class="form-group">
    {!! Form::label('payments', 'Payments:') !!}
    <p>{!! $requisitionForm->payments !!}</p>
</div>

