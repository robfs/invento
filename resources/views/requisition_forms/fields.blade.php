<!-- hidden user id field -->

{!! Form::number('user_id', $user['id'], ['class' => 'hidden form-control']) !!}
{!! Form::text('user_name', $user['name'], ['class' => 'hidden form-control']) !!}

<!-- Purpose Field -->
<div class="form-group col-sm-12 col-lg-12">
        {!! Form::label('item_required', 'Item Required:') !!}
        {!! Form::textarea('item_required', null, ['class' => 'form-control', 'required' => 'required']) !!}
    </div>

<!-- Date Needed Field -->
<div class="form-group col-sm-6">
    {!! Form::label('date_needed', 'Date Needed:') !!}
    {!! Form::date('date_needed', null, ['class' => 'form-control','id'=>'date_needed', 'required' => 'required']) !!}
</div>

@section('scripts')
    <script type="text/javascript">
        $('#date_needed').datetimepicker({
            format: 'YYYY-MM-DD',
            useCurrent: false
        })
    </script>
@endsection

<!-- Department Field -->
<div class="form-group col-sm-6">
    {!! Form::label('department', 'Department:') !!}
    {!! Form::select('department', ['The Planning Handbook' => 'The Planning Handbook', 'The Travelling Wine Merchant' => 'The Travelling Wine Merchant', 'The New Wine Company' => 'The New Wine Company', 'SEO' => 'SEO', 'Websites' => 'Websites', 'Andrew House / Company-wide' => 'Andrew House / Company-wide'], null, ['class' => 'form-control', 'required' => 'required']) !!}
</div>

<!-- Purpose Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('purpose', 'Purpose of Purchase:') !!}
    {!! Form::textarea('purpose', null, ['class' => 'form-control', 'required' => 'required']) !!}
</div>

<!-- Vendor Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('vendor_name', 'Vendor Name:') !!}
    {!! Form::text('vendor_name', null, ['class' => 'form-control', 'required' => 'required']) !!}
</div>

<!-- Vendor Web Address Field -->
<div class="form-group col-sm-6">
    {!! Form::label('vendor_web_address', 'Vendor Web Address:') !!}
    {!! Form::text('vendor_web_address', null, ['class' => 'form-control', 'required' => 'required']) !!}
</div>

<!-- Product Code Field -->
<div class="form-group col-sm-6">
    {!! Form::label('product_code', 'Product Code:') !!}
    {!! Form::text('product_code', null, ['class' => 'form-control', 'required' => 'required']) !!}
</div>

<!-- Unit Price Field -->
<div class="form-group col-sm-6">
    {!! Form::label('unit_price', 'Unit Price (Net of VAT)(£):') !!}
    {!! Form::number('unit_price', null, ['class' => 'form-control', 'required' => 'required', 'step' => '0.01']) !!}
</div>

<?php $user = Auth::user() ?>

@if($user->hasRole('Admin')) 
<?php $cols = 'col-sm-4'; ?>
@else
<?php $cols = 'col-sm-6'; ?>
@endif

<!-- Qty Field -->
<div class="form-group <?php echo $cols; ?>">
    {!! Form::label('qty', 'Qty:') !!}
    {!! Form::number('qty', null, ['class' => 'form-control', 'required' => 'required']) !!}
</div>

<!-- Payments Field -->
<div class="form-group <?php echo $cols; ?>">
    {!! Form::label('payments', 'Payments:') !!}
    {!! Form::select('payments', ['One-Off Payment' => 'One-Off Payment', 'Monthly Payment' => 'Monthly Payment'], null, ['class' => 'form-control', 'required' => 'required']) !!}
</div>

@if($user->hasRole('Admin')) 
<!-- Payments Field -->
<div class="form-group <?php echo $cols; ?>">
    {!! Form::label('status', 'Status:') !!}
    {!! Form::select('status', ['Waiting' => 'Waiting', 'Approved' => 'Approved', 'Denied' => 'Denied'], null, ['class' => 'form-control', 'required' => 'required']) !!}
</div>
@endif

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
   <!-- <a href="{!! route('requisitionForms.create') !!}" class="btn btn-default">Cancel</a> -->
</div>
