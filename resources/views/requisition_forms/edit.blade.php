@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Requisition Form
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($requisitionForm, ['route' => ['requisitionForms.update', $requisitionForm->id], 'method' => 'patch']) !!}

                        @include('requisition_forms.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection