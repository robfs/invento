<?php

return [
    'configs' => [
        [
            'name' => 'woo-webhook',
            'signing_secret' => env('WOO_CLIENT_SECRET'),
            'signature_header_name' => 'x-wc-webhook-signature',
            'signature_validator' => App\Handler\WooValidateHandler::class,
            'webhook_profile' => \Spatie\WebhookClient\WebhookProfile\ProcessEverythingWebhookProfile::class,
            'webhook_model' => \Spatie\WebhookClient\Models\WebhookCall::class,
            'process_webhook_job' => App\Handler\WooWebhookHandler::class,
        ],
        [
            'name' => 'square-webhook',
            'signing_secret' => env('SQUARE_CLIENT_SECRET'),
            'signature_header_name' => 'x-square-signature',
            'signature_validator' => App\Handler\SquareValidateHandler::class,
            'webhook_profile' => \Spatie\WebhookClient\WebhookProfile\ProcessEverythingWebhookProfile::class,
            'webhook_model' => \Spatie\WebhookClient\Models\WebhookCall::class,
            'process_webhook_job' => App\Handler\SquareWebhookHandler::class,
        ],
    ],
];
