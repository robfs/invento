<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Woocommerce;
use SquareConnect\Configuration as Square;
use SquareConnect\Api\V1ItemsApi as V1Items;
use Flash;
use App\Models\Inventory;

class SyncProductIdsController extends Controller
{
        /**
     * Display a listing of the Inventory.
     *
     * @param InventoryDataTable $inventoryDataTable
     * @return Response
     */
    public function index()
    {
        return view('wine.syncproducts.index');
    }
 
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

        $config = Square::getDefaultConfiguration();
        $config->setHost(env('SQUARE_API_URL'));
        $config->setAccessToken(env('SQUARE_API_ACCESS_TOKEN'));
        $apiInstance = new V1Items();
        $locationID = "07K13P3S2S4AV";
        $result = $apiInstance->listItems($locationID);
        $square_products = [];
        foreach($result as $item){

            foreach($item['variations'] as $square_product){
                $square_products[$square_product['item_id']] = [
                    'sku' => $square_product['sku'],
                    'item_id' => $square_product['item_id']
                ];
               
            }
        }
        foreach($square_products as $key => $sproduct){

            $invento_item = Inventory::where('sku', $sproduct['sku'])->first();
            
            if($invento_item){
                $invento_item->square_id = $sproduct['item_id'];
                $invento_item->save();
            }
          
        }

        return back();
     
    }


   /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($product)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($product)
    {
      

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($product)
    {
    
    }

}
