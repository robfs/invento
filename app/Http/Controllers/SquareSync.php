<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use SquareConnect\Api\V1ItemsApi as SAPI;
use SquareConnect\Configuration as Square;
use SquareConnect\Model\V1AdjustInventoryRequest as VAPI;
use Artisan;
class SquareSync extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($product)
    {
        //

        $config = Square::getDefaultConfiguration();
        $config->setHost(env('SQUARE_API_URL'));
        $config->setAccessToken(env('SQUARE_API_ACCESS_TOKEN'));
        $locationID = "07K13P3S2S4AV";
        $apiInstance = new SAPI();
        $result = $apiInstance->retrieveItem('07K13P3S2S4AV', $product['id']);
        $square_current_inventory = $apiInstance->listInventory($locationID);

        $product_qty = (int)$product['stock_qty'];
        //  dd($square_current_inventory);

        foreach ($result['variations'] as $square_product) {

            if ($square_product['sku'] === $product['sku']) {
                $var_id = $square_product['id'];

                foreach ($square_current_inventory as $sinventory) {

                    if ($var_id === $sinventory['variation_id']) {
                        $current_count = $sinventory['quantity_on_hand'];
                    }

                }

                if($product_qty < $current_count){
                    $diff = abs( $current_count - $product_qty);
                    $new_stock = $diff * -1; 
                }

                if($product_qty > $current_count){
                    $diff = abs( $current_count - $product_qty);
                    $new_stock = $diff;
                }

                if($product_qty === 0){
                    $diff = abs( $product_qty - $current_count);
                    $new_stock = $diff * -1; 
                }

                if($product_qty != $current_count){
                    $body = new VAPI();
                    $body->setQuantityDelta($new_stock);
                    $body->setAdjustmentType('MANUAL_ADJUST');
                    $result3 = $apiInstance->adjustInventory('07K13P3S2S4AV', $square_product['id'], $body);
                }

            }
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
