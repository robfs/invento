<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Woocommerce;
use Flash;

class WoocommerceSync extends Controller
{
 
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($request)
    {
        //

        $data = [
            'name' => $request->product_title,
            'type' => 'simple',
            'regular_price' => $request->price,
            'description' => $request->description,
            'short_description' => $request->short_description,
            'sku' => $request->sku,
            'categories' => [
                [
                    'id' => 9
                ],
                [
                    'id' => 14
                ]
            ],
            'images' => [
                [
                    'src' => 'http://demo.woothemes.com/woocommerce/wp-content/uploads/sites/56/2013/06/T_2_front.jpg'
                ],
                [
                    'src' => 'http://demo.woothemes.com/woocommerce/wp-content/uploads/sites/56/2013/06/T_2_back.jpg'
                ]
            ],
            'meta_data' => [
                [
                    'key' => 'cellaring',
                    'value' => '123'
                ],
                [
                    'key' => 'preservatives',
                    'value' => '123'
                ]
            ]
        ];

       $product = Woocommerce::post('products', $data);

       
        
    }


   /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($product)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($product)
    {
        //

        $params = [
            'sku' => $product['sku'],
        ];

        $wooProduct = Woocommerce::get('products', $params);

        if($wooProduct){
            $wooID = $wooProduct[0]['id'];

            $data =  [
                'stock_quantity' => $product['stock_qty']
            ];
           $done = Woocommerce::put('products/' . $wooID, $data);
        }

        Flash::success('Woocommerce / Square Inventory Updated Successfully');

        return back();
    
       // return Woocommerce::put('products/1', $data);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($product)
    {
        //
    }

}
