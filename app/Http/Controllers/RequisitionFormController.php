<?php

namespace App\Http\Controllers;

use App\DataTables\RequisitionFormDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateRequisitionFormRequest;
use App\Http\Requests\UpdateRequisitionFormRequest;
use App\Repositories\RequisitionFormRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use Auth;
use Carbon\Carbon; 
use Mail;
use App\Mail\OnlineRequisitionRequest;
use App\User;

class RequisitionFormController extends AppBaseController
{
    /** @var  RequisitionFormRepository */
    private $requisitionFormRepository;

    public function __construct(RequisitionFormRepository $requisitionFormRepo)
    {
        $this->requisitionFormRepository = $requisitionFormRepo;
        $this->middleware(['auth', 'role:Admin', 'clearance'])->except('create', 'store');
    }

    /**
     * Display a listing of the RequisitionForm.
     *
     * @param RequisitionFormDataTable $requisitionFormDataTable
     * @return Response
     */
    public function index(RequisitionFormDataTable $requisitionFormDataTable)
    {
        return $requisitionFormDataTable->render('requisition_forms.index');
    }

    /**
     * Show the form for creating a new RequisitionForm.
     *
     * @return Response
     */
    public function create()
    {
        return view('requisition_forms.create')->with('user',  Auth::User());
    }

    /**
     * Store a newly created RequisitionForm in storage.
     *
     * @param CreateRequisitionFormRequest $request
     *
     * @return Response
     */
    public function store(CreateRequisitionFormRequest $request)
    {
        $input = $request->all();

        $requisitionForm = $this->requisitionFormRepository->create($input);

        if(!Auth::User()->hasRole('Admin')){

            Flash::success('Requisition Form saved successfully.');

            return redirect(route('requisitionForms.create'));

        }

        Flash::success('Requisition Form saved successfully.');

        return redirect(route('requisitionForms.index'));

    }

    /**
     * Display the specified RequisitionForm.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $requisitionForm = $this->requisitionFormRepository->find($id);

        if (empty($requisitionForm)) {
            Flash::error('Requisition Form not found');

            return redirect(route('requisitionForms.index'));
        }

        return view('requisition_forms.show')->with('requisitionForm', $requisitionForm);
    }

    /**
     * Show the form for editing the specified RequisitionForm.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $requisitionForm = $this->requisitionFormRepository->find($id);
        $dateNeeded = new Carbon($requisitionForm->date_needed);
        $requisitionForm->date_needed = $dateNeeded->format('Y-m-d');

        $user = User::findOrFail($requisitionForm->user_id);

        if (empty($requisitionForm)) {
            Flash::error('Requisition Form not found');

            return redirect(route('requisitionForms.index'));
        }

        return view('requisition_forms.edit')->with(['requisitionForm' => $requisitionForm, 'user' => $user]);
    }

    /**
     * Update the specified RequisitionForm in storage.
     *
     * @param  int              $id
     * @param UpdateRequisitionFormRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRequisitionFormRequest $request)
    {
        $requisitionForm = $this->requisitionFormRepository->find($id);
        $user = User::findOrFail($requisitionForm->user_id);
        
        if (empty($requisitionForm)) {
            Flash::error('Requisition Form not found');
            return redirect(route('requisitionForms.index'));
        }

        $requisitionForm = $this->requisitionFormRepository->update($request->all(), $id);

        if($requisitionForm->status != 'Approved'){
            $this->emailStatus($requisitionForm);
        }
    
        Flash::success('Requisition Form updated successfully.');

        return redirect(route('requisitionForms.index'));
    }

    /**
     * Remove the specified RequisitionForm from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $requisitionForm = $this->requisitionFormRepository->find($id);

        if (empty($requisitionForm)) {
            Flash::error('Requisition Form not found');

            return redirect(route('requisitionForms.index'));
        }

        $this->requisitionFormRepository->delete($id);

        Flash::success('Requisition Form deleted successfully.');

        return redirect(route('requisitionForms.index'));
    }

    private function emailStatus($requisitionForm){

        $user = User::findOrFail($requisitionForm->user_id);

        if($user){

            $data = [
                "email" => $user->email,
                "username" => $user->name,
            ]; 
    
            if($requisitionForm === 'Approved'){
                Mail::to($data['email'])->send(new OnlineRequisitionRequest($data));
    
            }elseif($status === 'Denied'){
    
    
            }else{
    
    
            }

        }

    }
}
