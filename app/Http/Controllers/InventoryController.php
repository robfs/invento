<?php

namespace App\Http\Controllers;

use App\DataTables\InventoryDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateInventoryRequest;
use App\Http\Requests\UpdateInventoryRequest;
use App\Repositories\InventoryRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use Intervention\Image\Facades\Image;
use App\Http\Controllers\WoocommerceSync;
use Woocommerce;

class InventoryController extends AppBaseController
{
    /** @var  InventoryRepository */
    private $inventoryRepository;

    public function __construct(InventoryRepository $inventoryRepo)
    {
        $this->inventoryRepository = $inventoryRepo;
        $this->middleware(['auth', 'role:Admin|Wine']);
    }

    /**
     * Display a listing of the Inventory.
     *
     * @param InventoryDataTable $inventoryDataTable
     * @return Response
     */
    public function index(InventoryDataTable $inventoryDataTable)
    {
        return $inventoryDataTable->render('wine.inventories.index');
    }

    /**
     * Show the form for creating a new Inventory.
     *
     * @return Response
     */
    public function create()
    {
        return view('wine.inventories.create');
    }

    /**
     * Store a newly created Inventory in storage.
     *
     * @param CreateInventoryRequest $request
     *
     * @return Response
     */
    public function store(CreateInventoryRequest $request)
    {
        $input = $request->all();

        if($request->hasFile('featured_image')){
     
            $featured = $request->file('featured_image');
            $filename = $featured->getClientOriginalName();
            Image::make($featured)->save( public_path('/uploads/product_images/' . $filename ) );
            $input['featured_image'] = $filename;
        }
        
        $inventory = $this->inventoryRepository->create($input);

        $params = [
            'sku' => $request->sku,
        ];

      //  $product_exists = Woocommerce::get('products', $params);
        
      //  if(!$product_exists){
      //      $sync = new WoocommerceSync;
      //      $sync->create($request);
      //  }

        Flash::success('Inventory saved successfully.');

        return redirect(route('wine.inventories.index'));
    }

    /**
     * Display the specified Inventory.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $inventory = $this->inventoryRepository->find($id);

        if (empty($inventory)) {
            Flash::error('Inventory not found');

            return redirect(route('wine.inventories.index'));
        }

        return view('wine.inventories.show')->with('inventory', $inventory);
    }

    /**
     * Show the form for editing the specified Inventory.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $inventory = $this->inventoryRepository->find($id);

        if (empty($inventory)) {
            Flash::error('Inventory not found');

            return redirect(route('wine.inventories.index'));
        }

        return view('wine.inventories.edit')->with('inventory', $inventory);
    }

    /**
     * Update the specified Inventory in storage.
     *
     * @param  int              $id
     * @param UpdateInventoryRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateInventoryRequest $request)
    {
        $input = $request->all();

        $inventory = $this->inventoryRepository->find($id);

        if (empty($inventory)) {
            Flash::error('Inventory not found');

            return redirect(route('wine.inventories.index'));
        }

        if($request->hasFile('featured_image')){
            $featured = $request->file('featured_image');
            $filename = $featured->getClientOriginalName();
            Image::make($featured)->save( public_path('/uploads/product_images/' . $filename ) );
            $input['featured_image'] = $filename;
        }

    
        Flash::success('Inventory updated successfully.');

     
        if($inventory->stock_qty != (int)$request['stock_qty']){
            
            $inventoryProduct = [
                'id' => $inventory->square_id,
                'sku' => $inventory->sku,
                'stock_qty' => $request['stock_qty'],
            ];
    
            $wooSync = new WoocommerceSync(); 
            $wooSync->update($inventoryProduct);
            // Sync square
            $squareSync = new SquareSync();
            $squareSync->update($inventoryProduct);
            
        }

        $inventory = $this->inventoryRepository->update($input, $id);

        
        //Sync woocommerce 

        return redirect(route('inventories.index'));
    }

    /**
     * Remove the specified Inventory from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $inventory = $this->inventoryRepository->find($id);

        if (empty($inventory)) {
            Flash::error('Inventory not found');

            return redirect(route('inventories.index'));
        }
        

        $this->inventoryRepository->delete($id);

        Flash::success('Inventory deleted successfully.');

        return redirect(route('inventories.index'));
    }
}
