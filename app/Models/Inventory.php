<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Inventory
 * @package App\Models
 * @version September 17, 2019, 9:54 am UTC
 *
 * @property string sku
 * @property string product_title
 * @property string description
 * @property string short_description
 * @property float price
 * @property float sale_price
 * @property string featured_image
 * @property string gallery_images
 */
class Inventory extends Model
{

    public $table = 'inventory';
    


    public $fillable = [
        'sku',
        'square_id',
        'product_title',
        'description',
        'short_description',
        'price',
        'sale_price',
        'featured_image',
        'gallery_images',
        'stock_qty',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'price' => 'float',
        'sale_price' => 'float'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'sku' => 'required',
        'product_title' => 'required',
        'price' => 'required'
    ];

    
}
