<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class RequisitionForm
 * @package App\Models
 * @version October 8, 2019, 2:10 pm UTC
 *
 * @property integer user_id
 * @property string date_needed
 * @property string department
 * @property string purpose
 * @property string vendor_name
 * @property string vendor_web_address
 * @property string product_code
 * @property string unit_price
 * @property integer qty
 * @property string payments
 */
class RequisitionForm extends Model
{

    public $table = 'requisition_forms';
    


    public $fillable = [
        'user_id',
        'user_name',
        'item_required',
        'date_needed',
        'department',
        'purpose',
        'vendor_name',
        'vendor_web_address',
        'product_code',
        'unit_price',
        'qty',
        'payments',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'user_id' => 'integer',
        'qty' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'department' => 'required'
    ];

    
}
