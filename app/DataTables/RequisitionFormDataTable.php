<?php

namespace App\DataTables;

use App\Models\RequisitionForm;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Carbon\Carbon;

class RequisitionFormDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        $dataTable->editColumn('created_at', function ($data) {
            return $data->created_at ? with(new Carbon($data->created_at))->format('m/d/Y') : '';
        });

        $dataTable->editColumn('date_needed', function ($data) {
            return $data->date_needed ? with(new Carbon($data->date_needed))->format('m/d/Y') : '';
        });

        $dataTable->editColumn('unit_price', function ($data) {
            return $data->unit_price ? '£' . $data->unit_price : '';
        });
        
        return $dataTable->addColumn('action', 'requisition_forms.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\RequisitionForm $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(RequisitionForm $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false])
            ->parameters([
                'dom'       => 'Bfrtip',
                'stateSave' => true,
                'order'     => [[0, 'desc']],
                'buttons'   => [
                    ['extend' => 'create', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'export', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'print', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'reset', 'className' => 'btn btn-default btn-sm no-corner',],
                    ['extend' => 'reload', 'className' => 'btn btn-default btn-sm no-corner',],
                ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'user_name',
            'item_required',
            'department',
            'unit_price',
            'date_needed',
            'created_at',
            'status'
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'requisition_formsdatatable_' . time();
    }
}
