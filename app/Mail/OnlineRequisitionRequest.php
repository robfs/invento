<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class OnlineRequisitionRequest extends Mailable
{
    use Queueable, SerializesModels;

    public $username = '';
    public $email = ''; 

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        //

        if($data){

            $this->username = $data['username'];
            $this->email = $data['email'];
            
        }
      

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('maileclipse::templates.onlineRequisitionForm');
    }
}
