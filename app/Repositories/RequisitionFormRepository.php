<?php

namespace App\Repositories;

use App\Models\RequisitionForm;
use App\Repositories\BaseRepository;

/**
 * Class RequisitionFormRepository
 * @package App\Repositories
 * @version October 8, 2019, 2:10 pm UTC
*/

class RequisitionFormRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'user_name',
        'date_needed',
        'department',
        'vendor_name',
        'vendor_web_address',
        'product_code',
        'unit_price',
        'payments'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return RequisitionForm::class;
    }
}
