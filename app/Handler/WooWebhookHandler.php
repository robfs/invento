<?php 

namespace App\Handler;

use Spatie\WebhookClient\ProcessWebhookJob;
use Illuminate\Support\Facades\Log;
use App\Models\Inventory;
use App\Http\Controllers\SquareSync;

class WooWebhookHandler extends ProcessWebhookJob
{

    public function handle(){

        Artisan::call('config:clear');
        Artisan::call('cache:clear');

       $data = json_decode($this->webhookCall, true)['payload'];
       $found = Inventory::where('sku', $data['sku'])->first();

       if($found){
        $found->stock_qty = $data['stock_quantity'];
        $found->save();

        $product = [
            'id' => $found->square_id,
            'sku' => $found->sku,
            'stock_qty' => $found->stock_qty,
        ];

        $squareSync = new SquareSync();
        $squareSync->update($product);

       }

      
    }


}