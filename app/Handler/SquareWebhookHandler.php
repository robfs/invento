<?php 

namespace App\Handler;

use Spatie\WebhookClient\ProcessWebhookJob;
use Illuminate\Support\Facades\Log;
use SquareConnect\Configuration as Square;
use SquareConnect\Api\CatalogApi as SAPI;
use App\Models\Inventory;
use App\Http\Controllers\WoocommerceSync;

class SquareWebhookHandler extends ProcessWebhookJob
{

    public function handle(){

        Artisan::call('config:clear');
        Artisan::call('cache:clear');
        $data = json_decode($this->webhookCall, true)['payload'];
        $config = Square::getDefaultConfiguration();
        $config->setHost(env('SQUARE_API_URL'));
        $config->setAccessToken(env('SQUARE_API_ACCESS_TOKEN'));
        $product = [];
        
        $updated_items = isset($data['data']['object']['inventory_counts']) ? $data['data']['object']['inventory_counts'] : NULL;

        if($updated_items != NULL) {
            
            foreach($updated_items as $item){
                $product['cat_id'] = $item['catalog_object_id'];
                $product['qty'] = $item['quantity'];
                $apiInstance = new SAPI();
                try {
                    $result = $apiInstance->retrieveCatalogObject($product['cat_id']);
                    $data = $result->getObject();
                    $product['sku'] = $data['item_variation_data']['sku'];
    
                    $found = Inventory::where('sku', $product['sku'])->first();
                    if($found){
                        $found->stock_qty = $product['qty'];
                        $found->save();
    
                        $wooSync = new WoocommerceSync(); 
                        $wooSync->update($found);
    
                    }
                    
                } catch (Exception $e) {
                    log::info($e->message);
                }
    
            }
        }
    
    }


}